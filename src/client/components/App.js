import React, { useEffect, useState } from 'react';
import {
  Switch,
  Route,
  HashRouter as Router,
} from 'react-router-dom';
import { useDebounce } from 'use-debounce';
import { GlobalStyle } from '../utils/styled';
import { fetchPopularMovies, searchMovies } from '../utils/fetcher';
import PopularMovies from './PopularMovies';
import MovieDetail from './MovieDetail';
import Search from './Search';

const App = () => {
  const [movies, setMovies] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const setSearch = e => setSearchQuery(e.target.value);
  const [debouncedSearch] = useDebounce(searchQuery, 200);

  useEffect(() => {
    let didCancel = false;
    let didCancelSearch = false;
    async function fetchPopular() {
      try {
        const queryResult = await fetchPopularMovies();
        if (!didCancel) {
          setMovies(queryResult);
        }
      } catch (error) {
        if (!didCancel) {
          console.error(error);
        }
      }
    }
    async function search() {
      try {
        const movieSearch = await searchMovies(debouncedSearch);
        if (!didCancelSearch) {
          setMovies(movieSearch);
        }
      } catch (error) {
        if (!didCancelSearch) {
          console.error(error);
        }
      }
    }

    if (debouncedSearch) {
      search();
    } else {
      fetchPopular();
    }
    return () => {
      didCancel = true;
      didCancelSearch = true;
    };
  }, [debouncedSearch]);

  return (
    <Router baseName="/">
      <GlobalStyle />
      <Switch>
        <Route exact path="/">
          <div style={{ margin: '1rem' }}>
            <h1>Popular movies</h1>
            <Search setSearch={setSearch} search={searchQuery} />
            <PopularMovies movies={movies} />
          </div>
        </Route>
        <Route path="/movie/:id">
          <MovieDetail />
        </Route>
      </Switch>
    </Router>
  );
};
export default App;
