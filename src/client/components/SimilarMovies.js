import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getSimilarMovies } from '../utils/fetcher';
import { SimilarWrapper, FancyLink } from '../utils/styled';

const cache = new Map();

const SimilarMovies = ({ movieId }) => {
  const [similar, setSimilar] = useState([]);
  useEffect(() => {
    let didCancel = false;
    async function getRelatedMovies() {
      if (cache.has(movieId)) {
        setSimilar(cache.get(movieId));
        return;
      }
      try {
        const similarMovies = await getSimilarMovies(movieId);
        if (!didCancel) {
          setSimilar(similarMovies);
          cache.set(movieId, similarMovies);
        }
      } catch (error) {
        if (!didCancel) {
          console.error(error);
        }
      }
    }
    getRelatedMovies();
    return () => {
      didCancel = true;
    };
  }, [movieId]);

  return (
    <SimilarWrapper>
      <h3>Similar movies</h3>
      {
      similar.map(item => <div key={item.id} style={{ margin: '1rem 0' }}><FancyLink to={`/movie/${item.id}`}>{item.title}</FancyLink></div>)
    }
    </SimilarWrapper>
  );
};

SimilarMovies.propTypes = {
  movieId: PropTypes.string.isRequired,
};

export default SimilarMovies;
