import React from 'react';
import PropTypes from 'prop-types';
import Movie from './Movie';
import { MovieCard } from '../utils/styled';

const PopularMovies = ({ movies }) => (
  <div style={{ margin: '1rem ' }}>
    {
      movies.length > 0
        ? movies.map(movie => <Movie key={movie.id} movie={movie} />)
        : <MovieCard>Oops! No results found.</MovieCard>
    }

  </div>
);

PopularMovies.propTypes = {
  movies: PropTypes.instanceOf(Array).isRequired,
};

export default PopularMovies;
