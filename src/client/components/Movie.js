import React from 'react';
import PropTypes from 'prop-types';
import { PlainLink, MovieCard } from '../utils/styled';

const IMAGE_BASE = 'https://image.tmdb.org/t/p/w500';

const Movie = ({ movie }) => (
  <PlainLink to={`/movie/${movie.id}`}>
    <MovieCard>
      {
          movie.poster_path
            ? <img alt="poster" style={{ width: '100%', maxWidth: '5rem' }} title="movie poster" src={`${IMAGE_BASE}${movie.poster_path}`} />
            : null
        }
      <div style={{ padding: '1rem' }}>
        <h3>{movie.title}</h3>
        <div>{movie.overview}</div>
      </div>
    </MovieCard>
  </PlainLink>
);

Movie.propTypes = {
  movie: PropTypes.instanceOf(Object).isRequired,
};


export default Movie;
