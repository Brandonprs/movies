/* eslint-disable react/no-array-index-key */
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Ratings from 'react-ratings-declarative';
import { FancyLink, MovieDetailWrapper } from '../utils/styled';
import { getMovieDetails } from '../utils/fetcher';
import Credits from './Credits';
import SimilarMovies from './SimilarMovies';

const IMAGE_BASE = 'https://image.tmdb.org/t/p/w500';
const cache = new Map();
const dateOptions = {
  weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'
};

const MovieDetail = () => {
  const [details, setDetails] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const { id } = useParams();
  useEffect(() => {
    let didCancel = false;
    async function getDetails() {
      try {
        if (cache.has(id)) {
          setDetails(cache.get(id));
          return;
        }
        setIsLoading(true);
        const movieDetails = await getMovieDetails(id);
        if (!didCancel) {
          setDetails(movieDetails);
          cache.set(id, movieDetails);
        }
      } catch (error) {
        if (!didCancel) {
          console.error(error);
        }
      } finally {
        setIsLoading(false);
      }
    }
    getDetails();
    return () => {
      didCancel = true;
    };
  }, [id]);

  const date = new Date(details.release_date).toLocaleDateString(dateOptions);

  if (isLoading) return <MovieDetailWrapper>Loading...</MovieDetailWrapper>;
  return (
    <>
      <SimilarMovies movieId={id} />
      <MovieDetailWrapper>
        <FancyLink to="/">
        All Movies
        </FancyLink>
        <h1>{details.title}</h1>
        <h3>{details.tagline}</h3>
        <Ratings
          widgetDimensions="1.5rem"
          widgetSpacings="0.2rem"
          rating={details.vote_average / 2}
        >
          {
          [...Array(4)].map((num, idx) => <Ratings.Widget key={idx} widgetRatedColor="black" />)
        }
          <Ratings.Widget />
        </Ratings>
        <br />
        <img alt="poster" style={{ width: '100%', maxWidth: '35rem' }} title="movie poster" src={`${IMAGE_BASE}${details.poster_path}`} />
        <br />
        <div>
        Release date:
          {' '}
          {date}
        </div>
        <br />
        <p style={{ maxWidth: '35rem' }}>{details.overview}</p>
        <h2>Cast:</h2>
        <Credits movieId={id} />
      </MovieDetailWrapper>
    </>
  );
};

export default MovieDetail;
