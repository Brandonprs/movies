import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getMovieCredits } from '../utils/fetcher';
import { CreditsWrapper } from '../utils/styled';

const cache = new Map();

const Credits = ({ movieId }) => {
  const [cast, setCast] = useState([]);
  useEffect(() => {
    let didCancel = false;
    async function getCredits() {
      try {
        if (cache.has(movieId)) {
          setCast(cache.get(movieId));
          return;
        }
        const movieCredits = await getMovieCredits(movieId);
        if (!didCancel) {
          setCast(movieCredits.cast);
          cache.set(movieId, movieCredits.cast);
        }
      } catch (error) {
        if (!didCancel) {
          console.error(error);
        }
      }
    }
    getCredits();
    return () => {
      didCancel = true;
    };
  }, [movieId]);

  return (
    <CreditsWrapper>
      <h4>Character</h4>
      <h4>Actor</h4>
      {cast.slice(0, 10).map(member => (
        <React.Fragment key={member.id}>
          <span>
            {member.character}
          </span>
          <span>{member.name}</span>
        </React.Fragment>
      ))
      }
    </CreditsWrapper>
  );
};

Credits.propTypes = {
  movieId: PropTypes.string.isRequired,
};

export default Credits;
