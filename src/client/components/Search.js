/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SearchContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const SearchBar = styled.input`
  width: 50rem;
  padding: .375rem .75rem;
  border: none;
  border-radius: 5px;
  height: 2.3rem;
  font-size: 1.2rem;
`;

const Search = ({ setSearch, search }) => (
  <SearchContainer>
    <label htmlFor="search">
      <h2>Search by title</h2>
    </label>
    <SearchBar type="text" id="search" name="search" onChange={setSearch} value={search} />
  </SearchContainer>
);

Search.propTypes = {
  setSearch: PropTypes.func.isRequired,
  search: PropTypes.string.isRequired,
};

export default Search;
