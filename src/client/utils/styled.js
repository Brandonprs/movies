import styled, { createGlobalStyle } from 'styled-components';
import { Link } from 'react-router-dom';

export const GlobalStyle = createGlobalStyle`
  body {
    background-color: hsl(220, 10%, 90%);
    font-family: IBM Plex Sans;
  }

  h1 {
    padding: 1rem;
  }
`;

export const MovieDetailWrapper = styled.div`
  display: grid;
  justify-content: center;
  justify-items: center;
`;

export const FancyLink = styled(Link)`
  color: hsl(227,90%,50%);
  display: inline-block;
  position: relative;
  text-decoration: none;
  &:before {
    background-color: hsl(227,90%,50%);
    content: '';
    height: 2px;
    position: absolute;
    bottom: -1px;
    transition: width 0.3s ease-in-out;
    width: 100%;
  }
  &:hover {
    &:before {
      width: 0;
    }
  }
`;

export const CreditsWrapper = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-column-gap: 4rem;
`;

export const MovieCard = styled.div`
  display: flex;
  background-color: #FFF;
  box-shadow: 0px 4px 4px hsla(220, 10%, 40%, 0.05);
  padding: 1rem;
  margin: 1rem 0;
`;

export const PlainLink = styled(Link)`
  text-decoration: none;
  color: hsl(220, 10%, 10%);
`;

export const SimilarWrapper = styled.div`
  position: absolute;
  margin: 15rem 0 0 6rem;
  width: 20rem; 
`;
