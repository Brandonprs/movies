import axios from 'axios';

const BASE_URL = 'http://localhost:8080';

const axiosFetch = axios.create({ baseURL: BASE_URL });

export const fetchPopularMovies = async () => {
  try {
    const query = await axiosFetch({
      method: 'get',
      url: '/movies/popular',
    });
    return query.data.results;
  } catch (error) {
    return console.error(error);
  }
};

export const searchMovies = async (query) => {
  try {
    const searchQuery = await axiosFetch({
      method: 'get',
      url: `/movies/search?title=${query}`,
    });
    return searchQuery.data.results;
  } catch (error) {
    return console.error(error);
  }
};

export const getMovieDetails = async (id) => {
  try {
    const details = await axiosFetch({
      method: 'get',
      url: `/movies/${id}`,
    });
    return details.data;
  } catch (error) {
    return console.error(error);
  }
};

export const getMovieCredits = async (id) => {
  try {
    const details = await axiosFetch({
      method: 'get',
      url: `/movies/${id}/credits`,
    });
    return details.data;
  } catch (error) {
    return console.error(error);
  }
};

export const getSimilarMovies = async (id) => {
  try {
    const details = await axiosFetch({
      method: 'get',
      url: `/movies/${id}/similar`,
    });
    return details.data.results;
  } catch (error) {
    return console.error(error);
  }
};
