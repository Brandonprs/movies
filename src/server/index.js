const express = require('express');
const cors = require('cors');
const controller = require('./controller');

const app = express();

app.use(cors());

app.use(express.static('dist'));
app.get('/movies/popular', controller.getPopularMovies);
app.get('/movies/search', controller.searchMoviesByTitle);
app.get('/movies/:id', controller.getMoveDetails);
app.get('/movies/:id/credits', controller.getMovieCredits);
app.get('/movies/:id/similar', controller.getSimilarMovies);

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
