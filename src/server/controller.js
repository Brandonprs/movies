const axios = require('axios');

const MOVIE_BASE_URL = 'https://api.themoviedb.org/3';
const MOVIE_API_KEY = 'api_key=9cb3ad108140fec04cbb2a16cb23ec50';

const controller = (function main() {
  async function getPopularMovies(req, res, next) {
    try {
      const { data } = await axios.get(`${MOVIE_BASE_URL}/movie/popular?${MOVIE_API_KEY}`);
      res.json(data);
    } catch (error) {
      next(error);
    }
  }

  async function searchMoviesByTitle(req, res, next) {
    try {
      if (!req.query.title) {
        throw new Error('Invalid search');
      }
      const { data } = await axios.get(
        `${MOVIE_BASE_URL}/search/movie?${MOVIE_API_KEY}&query=${req.query.title}`
      );
      res.json(data);
    } catch (error) {
      next(error);
    }
  }

  async function getMoveDetails(req, res, next) {
    try {
      if (!req.params.id) {
        throw new Error('Movie ID must be defined');
      }
      const { data } = await axios.get(
        `${MOVIE_BASE_URL}/movie/${req.params.id}?${MOVIE_API_KEY}`
      );
      res.json(data);
    } catch (error) {
      next(error);
    }
  }

  async function getMovieCredits(req, res, next) {
    try {
      if (!req.params.id) {
        throw new Error('Movie ID must be defined');
      }
      const { data } = await axios.get(
        `${MOVIE_BASE_URL}/movie/${req.params.id}/credits?${MOVIE_API_KEY}`
      );
      res.json(data);
    } catch (error) {
      next(error);
    }
  }

  async function getSimilarMovies(req, res, next) {
    try {
      if (!req.params.id) {
        throw new Error('Movie ID must be defined');
      }
      const { data } = await axios.get(
        `${MOVIE_BASE_URL}/movie/${req.params.id}/similar?${MOVIE_API_KEY}`
      );
      res.json(data);
    } catch (error) {
      next(error);
    }
  }

  return Object.freeze({
    getPopularMovies,
    searchMoviesByTitle,
    getMoveDetails,
    getMovieCredits,
    getSimilarMovies,
  });
}());

module.exports = controller;
