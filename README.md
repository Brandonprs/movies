# Movies
A simple movie application using React, Node, and Express that lists most popular movies, and gives details for individual movies.

Boilerplate taken from [simple-react-full-stack](https://github.com/crsandeep/simple-react-full-stack).

# Quick Start
```
# Clone the repository
git clone https://github.com/Brandonprs/movies.git

# Go inside the directory
cd movies

# Install dependencies
yarn (or npm install)

# Start development server
yarn dev (or npm run dev)

# Build for production
yarn build (or npm run build)

# Start production server
yarn start (or npm start)
```
# Main Project Structure
```
- public
  - index.html
- src
  - client
    - components
    - utils
    - index.js
  - server
    - controller.js
    - index.js
```

To run a production build of the application, run
```
yarn
yarn build
yarn start
```
and point your browser to `http://localhost:3000`

Happy searching!